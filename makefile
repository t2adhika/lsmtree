GPP = g++
FLAGS = -O3 -g
FLAGS += -std=c++17
FLAGS += -fopenmp
LDFLAGS = -lpthread

all: main

.PHONY: main
main:
	$(GPP) $(FLAGS) -o $@.out $@.cpp $(LDFLAGS)

clean:
	rm -f *.out 