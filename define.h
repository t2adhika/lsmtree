#pragma once

#ifndef MAX_THREADS
#define MAX_THREADS 256
#endif

#ifndef MAX_CLIENT
#define MAX_CLIENT 256
#endif

#ifndef PADDING_BYTES
#define PADDING_BYTES 64
#endif

#ifndef SERVER_PORT
#define SERVER_PORT 8421
#endif

#ifndef INSERT_BUFFER_SIZE
#define INSERT_BUFFER_SIZE 1000
#endif

#ifndef SOCKET_DATA_SIZE
#define SOCKET_DATA_SIZE 1024
#endif

#ifndef SKIP_LIST_NEXT_PTR_INTERVAL
#define SKIP_LIST_NEXT_PTR_INTERVAL 10
#endif

#ifndef DEBUG
#define DEBUG if(0)
#define DEBUG1 if(0)
#define DEBUG2 if(0)
#endif

#ifndef VERBOSE
#define VERBOSE if(0)
#endif

#ifndef TRACE
#define TRACE if(0)
#endif

#ifndef TPRINT
#define TPRINT(str) cout<<"tid="<<tid<<": "<<str;
#endif

#define PRINT(name) { cout<<(#name)<<"="<<name<<endl; }