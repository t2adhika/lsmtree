#include <cmath>
#include <atomic>
#include <string>
using namespace std;

typedef struct insertRecord{
    string key;
    string value;
    chrono::time_point<std::chrono::high_resolution_clock> currentTime;
}insRecord;

class LSM {
private:
    
    int readThread;
    int writeThread;
    int flushThread;
    int compactor;
    int memtableSize;
    int cacheSize;
    int sizeRatio;
    int blockSize;
    
public:
    //atomic<int> insertPosition, insertCleaner, elementNumber;
    //insRecord *insertData;
    LSM(const int _rT, const int _wT, const int _fT, const int _cT, const int _mS, const int _cS, const int _sR, const int _bS);
    void startReadThreads();
    void startWriteThreads();
    void startCompactor();
};