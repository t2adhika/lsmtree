#include <cstdlib>
#include <LSM.h>
#include <cstring>
#include <string>
#include <mutex>

#include "define.h"
using namespace std;

class skiplist{
private:
    //size_t size;
    typedef struct skipRecord{
        string key;
        string value;
        skipRecord *next;
        skipRecord *nextN;
        mutex m;
        skipRecord(insRecord* R){
            key = R->key;
            value = R->value;
            next = NULL;
            nextN = NULL;
        }
    }skipRecord;
    skipRecord *head;
public:
    size_t size;
    skiplist();
    ~skiplist();
    void insert(insRecord* Record);
    string get(string key);
};

skiplist::skiplist(){
    insRecord *r = new insRecord();
    head = new skipRecord(r);
    head->key = "DummyKey";
    head->value = "DummyValue";
}
skiplist::~skiplist(){
    skipRecord *h = head;
    skipRecord *p;
    while(h!=NULL){
        p = h;
        h = h->next;
        delete p;
    }
}

void skiplist::insert(insRecord* Record){
    skipRecord* rec = new skipRecord(Record);
    if(head == NULL) head = rec; 
    else{
        skiplist::skipRecord *temp,*current,*previous;
        for(temp=head;temp->nextN!=NULL;temp = temp->nextN){
            if(temp->nextN->key.compare(Record->key)>0){
                break;
            }
        }
        temp->m.lock();
        int count = 0;
        for(current = temp, previous = NULL; current->next!=NULL; current = current->next){
            if(current->key.compare(Record->key)>0) break;
            previous = current;
            count++;
            if(count == SKIP_LIST_NEXT_PTR_INTERVAL){
                temp->nextN = current;
                temp->m.unlock();
                temp = current;
                temp->m.lock();
                count = 0;
            }
        }
        if(previous == NULL){
            rec->next = head;
            head = rec;
            temp->m.unlock();
        }
        else{
            previous->m.lock();
            current->m.lock();
            previous->next = rec;
            rec->next = current;
            if(count>=SKIP_LIST_NEXT_PTR_INTERVAL){
                rec->nextN = temp->nextN;
                temp->nextN = rec;
            }
            current->m.unlock();
            previous->m.unlock();
            temp->m.unlock();
        }
    }
    size += Record->key.length() + Record->value.length();
}

string skiplist::get(string key){
    skiplist::skipRecord *temp, *prev, *ptr;
    for(temp=head, prev=head; temp!=NULL && temp->key.compare(key)<0;){
        prev = temp;
        temp = temp->nextN;
    }
    ptr = prev;
    do{
        if(ptr->key.compare(key)==0) return ptr->value;
    }while(ptr!=temp);
    return NULL;
}