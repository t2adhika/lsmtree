#include <iostream>
#include <cmath>
#include <cstring>

using namespace std;

int main(int argc, char **argv){
    if (argc == 1) {
        cout<<"USAGE: "<<argv[0]<<" [options]"<<endl;
        cout<<"Options:"<<endl;
        cout<<"    -rT [int]      number of [r]ead [t]hreads"<<endl;
        cout<<"    -wT [int]      number of [w]rite [t]hreads"<<endl;
        //cout<<"    -fT [int]      number of [f]lash [t]hreads"<<endl;
        cout<<"    -cT [int]      number of [c]ompactor [t]hreads"<<endl;
        cout<<"    -mS [int]      [m]emtable [s]ize in Mega bits"<<endl;
        cout<<"    -cS [int]      [c]ache [s]ize in Mega bits"<<endl;
        cout<<"    -bS [int]      [b]lock [s]ize in Kilo bytes"<<endl;
        cout<<"    -sR [int]      inter-level [s]ize [r]atio"<<endl;
        cout<<endl;
        cout<<"Example: "<<argv[0]<<" -rT 5 -wT 5 -cT 2 -mS 1024 -cS 100 -bs 4 -sR 10"<<endl;
        return 1;
    }
    int readThread = 2;
    int writeThread = 2;
    int flushThread = 1;
    int compactor = 1;
    int memtableSize = (int) pow(2,30);
    int cacheSize = 100 * (int) pow(2,20);
    int sizeRatio = 10;
    int blockSize = 8 * (int) pow(2,10);
    for(int argcount = 1;argcount<argc;argcount+=2) {
        if(strcmp(argv[argcount],"-rT") == 0){
            readThread = atoi(argv[argcount+1]);
        }
        else if(strcmp(argv[argcount],"-wT") == 0) {
            writeThread = atoi(argv[argcount+1]);
        }
        //number ofFlush thread is fixed to 1
        /*                                            
        else if(strcmp(argv[argcount],"-fT") == 0) {   
            flushThread = atoi(argv[argcount+1]);
        }
        */
        else if(strcmp(argv[argcount],"-cT") == 0) {
            compactor = atoi(argv[argcount+1]);
        }
        else if(strcmp(argv[argcount],"-mS") == 0) {
            memtableSize = atoi(argv[argcount+1]) * (int) pow(2,20);
        }
        else if(strcmp(argv[argcount],"-cS") == 0) {
            cacheSize = atoi(argv[argcount+1]) * (int) pow(2,20);
        }
        else if(strcmp(argv[argcount],"-sR") == 0) {
            sizeRatio = atoi(argv[argcount+1]);
        }
        else if(strcmp(argv[argcount],"-bS") == 0) {
            blockSize = atoi(argv[argcount+1]) * 8 * 1024;
        }
    }
    return 0;
}