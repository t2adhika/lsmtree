#include <string.h>
#include <thread>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sstream>

#include "LSM.h"
#include "define.h"
#include "util.h"

using namespace std;

atomic<int> insertPosition, insertCleaner, elementNumber;
insRecord *insertData;

void * socketThread(void *arg)
{
    int new_socket = *((int *)arg);
    //while (true) {
        char buffer[SOCKET_DATA_SIZE]={0};
        int valread = read( new_socket , buffer, SOCKET_DATA_SIZE);

        vector<string> parts = splitString(buffer);
        if(parts.at(0).compare("insert")){
            while(elementNumber >= INSERT_BUFFER_SIZE);
            int pos = (insertPosition++) % INSERT_BUFFER_SIZE;
            insertData[pos].key = parts.at(1);
            insertData[pos].value = parts.at(2);
            insertData[pos].currentTime = std::chrono::high_resolution_clock::now();
        }
        else if(parts.at(0).compare("delete")){
            while(elementNumber >= INSERT_BUFFER_SIZE);
            int pos = (insertPosition++) % INSERT_BUFFER_SIZE;
            insertData[pos].key = parts.at(1);
            insertData[pos].value = nullptr;
            insertData[pos].currentTime = std::chrono::high_resolution_clock::now();
        }
    //}
}

LSM::LSM(const int _rT, const int _wT, const int _fT, const int _cT, const int _mS, const int _cS, const int _sR, const int _bS)
: readThread(_rT), writeThread(_wT), flushThread(_fT), compactor(_cT), memtableSize(_mS), cacheSize(_cS), sizeRatio(_sR), blockSize(_bS)
{
    insertPosition = 0;
    insertCleaner =0;
    elementNumber = 0;
    insertData = new insRecord[INSERT_BUFFER_SIZE];

    //initialize the shared buffer where socket will read records to insert in memtable
    thread *listener = new thread([&]() { /* access all variables by reference */
            int server_fd, new_socket, valread;
            int opt = 1;
            pthread_t tid[MAX_CLIENT];
            
            struct sockaddr_in socketAddress;
            int addressLength = sizeof(socketAddress);
            if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
            { 
                perror("socket failed"); 
                exit(EXIT_FAILURE); 
            }
            if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) 
            { 
                perror("setsockopt"); 
                exit(EXIT_FAILURE);
            }
            socketAddress.sin_family = AF_INET; 
            socketAddress.sin_addr.s_addr = inet_addr("127.0.0.1"); 
            socketAddress.sin_port = htons( SERVER_PORT );
            
            if (bind(server_fd, (struct sockaddr *) &socketAddress, sizeof(socketAddress))<0) 
            { 
                perror("bind failed"); 
                exit(EXIT_FAILURE); 
            } 
            if (listen(server_fd, 50) < 0) 
            { 
                perror("listen"); 
                exit(EXIT_FAILURE); 
            }
            int curThreadCount = 0;
            while(true){
                new_socket = accept(server_fd, (struct sockaddr *)&socketAddress,  (socklen_t*)&addressLength);

                //Create a new thread for each client socket. main thread will be runnint to accept new request
                if( pthread_create(&tid[curThreadCount++], NULL, LSM::socketThread, &new_socket) != 0 ){ 
                    printf("Failed to create thread\n");
                }
                if (curThreadCount >= MAX_CLIENT){
                    curThreadCount = 0;
                    while(curThreadCount < MAX_CLIENT){
                        pthread_join(tid[curThreadCount++],NULL);
                    }
                    curThreadCount = 0;
                }
            }
        });
    //Start the read threads

}
LSM::~LSM(){
    delete insertData;
}